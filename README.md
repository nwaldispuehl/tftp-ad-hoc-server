# TFTP Ad Hoc Server

_Small, bare-bone TFTP server only implementing serving files (`RRQ`) from a single directory._ 

```
user@host:~$ ./tftp-ad-hoc-server --help
 
tftp-ad-hoc-server x.y.z
Nico Waldispühl (retorte.ch)
Small, bare-bone TFTP server only implementing serving files (`RRQ`) from a single directory.
 
USAGE:
    tftp-ad-hoc-server [FLAGS] [OPTIONS]
 
FLAGS:
        --debug      Shows more logging output, e.g. the contents of all packets.
        --help       Prints help information
    -V, --version    Prints version information
 
OPTIONS:
    -d, --directory <DIRECTORY_TO_SERVE>    The directory that should be served. [default: ./]
    -a, --address <ADDRESS>                 The IP address this server should serve from. [default: 0.0.0.0]
    -p, --port <PORT>                       The port to serve from. Note that you need to be root for ports smaller than
                                            1024. [default: 69]
```

## Introduction

Sometimes I just wanted to instantly serve a file via the TFTP (trivial file transfer protocol) without having to install and configure some daemon first; this command line program serves exactly that purpose.

This TFTP program is based on the original TFTP definition in `RFC 1350` (https://tools.ietf.org/html/rfc1350.html) but does not support any other client operation than the read request -- opcode `RRQ`.

## Use cases

My primary use cases:

### Upgrading IP phone

Most notably I wanted to upgrade the firmware of my Cisco IP telephone (model SPA 303) from my Linux computer; the provided installer appeared to not work on it. There is the possibility to point the telephone to a firmware image offered by a TFTP server however. This is done by adding the address of the firmware as URL parameter like this:

    TELEPHONE_URL/upgrade?tftp://tftp-server-ip/firmware.bin
     
### Testing PXE environment

To test a new 'Preboot eXecution Environment' (PXE, aka 'net boot') image without rolling it out to the production server first I just placed the respective files (e.g. `pxelinux.0`, or `pxelinux.cfg` ) in a local directory, started the `tftp-ad-hoc-server` in it and pointed the`next-server` property of the to be installed VM to the local machine.

## How to use

Get a binary for your operating system in the [tags](https://gitlab.com/nwaldispuehl/tftp-ad-hoc-server/tags) section. It is offered for the Linux, Microsoft Windows, and Apple Mac OS platforms. Download it and place the contained executable somewhere in your filesystem. 

To start serving the current directory on port `69` (the default TFTP port) enter the following command. (You need `sudo` to attach to such a low port.)

```
$ sudo ./tftp-ad-hoc-server

2018-03-23T22:13:50 [INFO] - Ad hoc TFTP server. Quit with 'Ctrl + c'.
2018-03-23T22:13:50 [INFO] - Listening for new requests on '0.0.0.0:69' in directory '/home/user/my_files/'.
```

As soon as a request arrives, the server prints some information to the console as he serves it:

```
2018-03-23T22:15:55 [INFO] - 127.0.0.1:48644 asks for file 'sample.file'.
2018-03-23T22:15:55 [INFO] - Sending '/home/user/my_files/sample.file' (size: 1337) to 127.0.0.1:48644.
```

To choose another port and directory, use the command line parameters, e.g. for serving the users desktop on port 9999:

```
$ ./tftp-ad-hoc-server -p 9999 -d ~/Desktop/

2018-03-23T22:21:38 [INFO] - Ad hoc TFTP server. Quit with 'Ctrl + c'.
2018-03-23T22:21:38 [INFO] - Listening for new requests on '0.0.0.0:9999' in directory '/home/user/Desktop/'.
```

Note that you don't have to `sudo` as the port is high enough now.

On the other platforms just choose the executable name accordingly.

## How to build / test

After cloning the project, run with the `cargo` package manager:

```
$ cargo run
```

Build:

```
$ cargo build
```

you'll find the built files in the `target/debug/` directory.


Execute tests:

```
$ cargo test
```

To package an archive for the respective operating system you're currently running, use the script from a bash-like environment:

```
$ ./build_and_package.sh
```
It creates the archive in the `target/` directory.
