#[macro_use]
extern crate log;
extern crate clap;
extern crate chrono;

use log::Metadata;
use log::Record;

use tftp::server::Server;

use clap::{App, Arg, ArgMatches};
use log::LevelFilter;
use chrono::Local;
use std::thread;

// Declare TFTP module.
mod tftp;


// Constants for the command line argument parser.
const PROGRAM_NAME: &str = env!("CARGO_PKG_NAME");
const PROGRAM_VERSION: &str = env!("CARGO_PKG_VERSION");
const PROGRAM_AUTHOR: &str = env!("CARGO_PKG_AUTHORS");
const PROGRAM_DESCRIPTION: &str = env!("CARGO_PKG_DESCRIPTION");

const ARG_ADDRESS: &str = "address";
const ARG_ADDRESS_VALUE_NAME: &str = "ADDRESS";
const ARG_ADDRESS_SHORT: &str = "a";
const ARG_ADDRESS_HELP: &str = "The IP address this server should serve from.";

const ARG_PORT: &str = "port";
const ARG_PORT_VALUE_NAME: &str = "PORT";
const ARG_PORT_SHORT: &str = "p";
const ARG_PORT_HELP: &str = "The port to serve from. Note that you need to be root for ports smaller than 1024.";

const ARG_DIRECTORY: &str = "directory";
const ARG_DIRECTORY_VALUE_NAME: &str = "DIRECTORY_TO_SERVE";
const ARG_DIRECTORY_SHORT: &str = "d";
const ARG_DIRECTORY_HELP: &str = "The directory that should be served.";

const ARG_DEBUG: &str = "debug";
const ARG_DEBUG_HELP: &str = "Shows more logging output, e.g. the contents of all packets.";

// Default values if arguments are omitted.
const DEFAULT_ADDRESS: &str = "0.0.0.0";
const DEFAULT_PORT: &str = "69";
const DEFAULT_DIRECTORY: &str = "./";


static CONSOLE_LOGGER: ConsoleLogger = ConsoleLogger;


fn main() {
    let arguments = evaluate_command_line_arguments();

    let address = arguments.value_of(ARG_ADDRESS).unwrap();
    let port: u32 = arguments.value_of(ARG_PORT).unwrap().parse().unwrap();
    let directory = arguments.value_of(ARG_DIRECTORY).unwrap();
    let debug = arguments.is_present(ARG_DEBUG);

    initialize_logging_with(debug);

    info!("Ad hoc TFTP server. Quit with 'Ctrl + c'.");

    let server = Server::from(address, port, directory);
    server.start();
}

fn evaluate_command_line_arguments<'a>() -> ArgMatches<'a> {
    App::new(PROGRAM_NAME)
        .version(PROGRAM_VERSION)
        .author(PROGRAM_AUTHOR)
        .about(PROGRAM_DESCRIPTION)
        .arg(Arg::with_name(ARG_ADDRESS)
            .value_name(ARG_ADDRESS_VALUE_NAME)
            .short(ARG_ADDRESS_SHORT)
            .long(ARG_ADDRESS)
            .default_value(DEFAULT_ADDRESS)
            .help(ARG_ADDRESS_HELP))
        .arg(Arg::with_name(ARG_PORT)
            .value_name(ARG_PORT_VALUE_NAME)
            .short(ARG_PORT_SHORT)
            .long(ARG_PORT)
            .default_value(DEFAULT_PORT)
            .help(ARG_PORT_HELP))
        .arg(Arg::with_name(ARG_DIRECTORY)
            .value_name(ARG_DIRECTORY_VALUE_NAME)
            .short(ARG_DIRECTORY_SHORT)
            .long(ARG_DIRECTORY)
            .default_value(DEFAULT_DIRECTORY)
            .help(ARG_DIRECTORY_HELP))
        .arg(Arg::with_name(ARG_DEBUG)
            .long(ARG_DEBUG)
            .help(ARG_DEBUG_HELP))
        .get_matches()
}


fn initialize_logging_with(debug: bool) {
    log::set_logger(&CONSOLE_LOGGER).unwrap();
    log::set_max_level(
        if debug { LevelFilter::Debug } else { LevelFilter::Info }
    );
}

/// The `ConsoleLogger` just prints logging statement to the standard out, i.e. the console.
#[derive(Debug)]
struct ConsoleLogger;

impl log::Log for ConsoleLogger {

    fn enabled(&self, _metadata: &Metadata) -> bool {
        // We decide about the level with the `set_max_level` method in the logging initializer above.
        true
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            println!("{} {:?} [{}] - {}", Local::now().format("%Y-%m-%dT%H:%M:%S%.3f"), thread::current().id(), record.level(), record.args());
        }
    }

    fn flush(&self) {
        // nop
    }
}
