use std::process::exit;
use std::net::UdpSocket;
use std::fs::File;
use std::io::Read;
use std::io::SeekFrom;
use std::io::Seek;
use std::time::Duration;

use tftp::message::*;
use tftp::codes::*;
use tftp::thread_manager::*;
use std::net::SocketAddr;
use std::fs;
use std::path::MAIN_SEPARATOR;

const U16_RANGE: u32 = 65536;
const BUFFER_SIZE: usize = 1024;
const BLOCK_SIZE: usize = 512;
const SENDING_SOCKET_TIMEOUT_SECONDS: u64 = 1;

/// The `Server` type encapsulates the TFTP functionality.
///
/// # Examples
/// ```
/// use tftp::server::Server;
///
/// let server = Server::from("0.0.0.0", 69, "./", false);
/// server.start();
/// ```
pub struct Server {
    address: String,
    port: u32,
    served_directory: String
}

impl Server {

    /// Instantiates a new `Server` instance with the provided properties.
    pub fn from(address: &str, port: u32, served_directory: &str) -> Server {
        Server { address: address.to_string(), port, served_directory: Server::normalize_directory(served_directory) }
    }

    /// Starts the actual serving until interrupted by an OS signal.
    pub fn start(&self) {
        let socket = UdpSocket::bind(format!("{}:{}", self.address, self.port));
        let socket = match socket {
            Ok(s) => s,
            Err(e) => {
                error!("Are you root? You need to be for binding ports below 1024... Error: {}", e);
                exit(1);
            }
        };

        let mut thread_pool = ThreadManager::new(8);

        loop {
            info!("Listening for new requests on '{}:{}' in directory '{}'.", self.address, self.port, self.served_directory);

            let (remote_address, request_result) = Server::wait_for_request_on(&socket);

            match request_result {
                Ok(message) => {
                    let address_copy = self.address.clone();
                    let served_directory_copy = self.served_directory.clone();
                    let remote_address_copy = remote_address;

                    // We are starting an asynchronous job with a copy of our parameters.
                    thread_pool.execute(move || Server::process(&address_copy, &served_directory_copy, remote_address_copy, message));
                },
                Err(error_message) => {
                    error!("Network call yielded error: {}", error_message);
                    Server::send_error(&socket, &remote_address, ErrorCode::NotDefined, "Did not understand request.");
                }
            }
        }
    }

    fn process(address: &str, served_directory: &str, remote_address: SocketAddr, message: Message) {
        // We are binding a new, random socket for client communication.
        let socket = UdpSocket::bind(format!("{}:{}", address, 0)).unwrap();

        match message {
            // We only know read request at the moment.
            Message::ReadRequest { filename, .. } => Server::serve_read_request(&socket, &remote_address, served_directory, &filename),

            // If an error is sent, we just swallow it.
            Message::ErrorMessage { .. } => (),

            // For any other message type at this stage an error is transmitted. If you want to implement them, you can do this here.
            _ => {
                info!("{} sends {}.", remote_address, message);
                warn!("Message type currently not implemented: '{}'. Not doing anything.", message);
                Server::send_error(&socket, &remote_address, ErrorCode::NotDefined, "Operation not implemented.");
            }
        }
    }

    fn serve_read_request(socket: &UdpSocket, remote_address: &SocketAddr, served_directory: &str, filename: &str) {
        info!("{} asks for file '{}'.", remote_address, filename);
        let cleaned_up_filename = Server::tidy(filename);
        let path = format!("{}{}", served_directory, cleaned_up_filename);

        // FIXME
        // We just open a file. Directory traversal is prevented by the tidying of the filename above.
        // A more elaborate mechanism would be to only offer the files of the served directory to choose from.
        let file = File::open(&path);
        match file {
            Ok(f) => Server::send_file(&socket, &remote_address, &path, f),
            Err(e) => {
                warn!("Can't load file '{}' due to: {}", path, e);
                Server::send_error(&socket, &remote_address, ErrorCode::FileNotFound, &format!("The file '{}' can not be served.", cleaned_up_filename));
            }
        }
    }

    fn send_file(socket: &UdpSocket, remote_address: &SocketAddr, path: &str, mut file: File) {
        info!("Sending '{}' (size: {}) to {}.", path, file.metadata().unwrap().len(), remote_address);

        let file_size = file.metadata().unwrap().len();
        let blocks: u32 = (file_size / BLOCK_SIZE as u64 + 1) as u32;

        let mut buffer: [u8; BLOCK_SIZE] = [0; BLOCK_SIZE];

        let mut i: u32 = 0;
        while i < blocks {
            let current_block = i % U16_RANGE + 1;

            file.seek(SeekFrom::Start(u64::from(i) * BLOCK_SIZE as u64)).unwrap();
            let bytes_read = file.read(&mut buffer).unwrap();

            let message = Message::DataMessage { block: current_block as u16, data: Vec::from(&buffer[0..bytes_read]) };
            Server::send(&socket, &remote_address, &message);

            // Waiting for the ack.
            Server::set_timeout_in(socket);
            let (_, ack_result) = Server::wait_for_request_on(socket);
            Server::reset_timeout_in(socket);

            match ack_result {
                Ok(message) => {
                    // If we got an actual message, check if it is an ACK.
                    match message {
                        Message::Acknowledgement { block } => {
                            // If yes, and if it is for the recently sent block, we go on to the next block...

                            match block {
                                b if b == (current_block as u16) => {
                                    i += 1;
                                },
                                _ => {
                                    // ...otherwise we send a respective error and quit.
                                    warn!("Wrong ACK received. Was waiting for # {} but got # {}.", current_block, block);
                                    Server::send_error(&socket, &remote_address, ErrorCode::UnknownTransferId, &format!("Block {} expected, but block {} received.", current_block, block));
                                    break;
                                }
                            }
                        },
                        _ => {
                            warn!("No ACK received. Instead: {}", message);
                            Server::send_error(&socket, &remote_address, ErrorCode::IllegalTftpOperation, "ACK expected.");
                            break;
                        }
                    }
                },

                Err(e) => {
                    match e.as_ref() {
                        // If the error is the result of a timeout, we just repeat sending the block...
                        "WouldBlock" => (),

                        // ...otherwise we terminate sending and issue an error to the receiver.
                        _ => {
                            warn!("No ACK received. Error: {:?}", e);
                            Server::send_error(&socket, &remote_address, ErrorCode::IllegalTftpOperation, "ACK expected.");
                            break;
                        }
                    }
                }
            }
        }
    }

    fn send_error(socket: &UdpSocket, remote_address: &SocketAddr, error_code: ErrorCode, message: &str) {
        let message = Message::ErrorMessage { error_code, error_message: String::from(message) };
        Server::send(&socket, &remote_address, &message);
    }

    fn wait_for_request_on(socket: &UdpSocket) -> (SocketAddr, Result<Message, String>) {
        let mut buffer = [0; BUFFER_SIZE];
        let (bytes_read, remote_address) = socket.recv_from(&mut buffer).unwrap();
        let decoded_buffer = MessageDecoder::decode(&buffer[0..bytes_read]);
        match decoded_buffer {
            Ok(message) => {
                debug!("{} -> {}", remote_address, message);
                (remote_address, Result::Ok(message))
            },
            Err(error_message) => (remote_address, Result::Err(error_message))
        }
    }

    fn send(socket: &UdpSocket, remote_address: &SocketAddr, message: &Message) {
        debug!("{} <- {}", remote_address, message);
        let error_payload = MessageEncoder::encode(message);
        socket.send_to(&error_payload, remote_address).unwrap();
    }

    /// Removes all double dots and backslashes from a string. Additionally we cut away all leading slashes (or, MAIN_SEPARATORS).
    fn tidy(resource: &str) -> String {
        resource.replace("\\", "").replace("..", "").trim_start_matches(MAIN_SEPARATOR).to_string()
    }

    /// We bring the directory to a absolute form.
    fn normalize_directory(directory: &str) -> String {
        Server::add_trailing_path_separator(&Server::canonicalize(directory))
    }

    /// We simply check if there is a trailing path separator. If not, we add one.
    fn add_trailing_path_separator(directory: &str) -> String {
        let mut result = directory.to_string();
        if !directory.ends_with(MAIN_SEPARATOR) {
            result.push(MAIN_SEPARATOR);
        }
        result
    }

    fn canonicalize(directory: &str) -> String {
        let canonicalized_path_result = fs::canonicalize(directory);
        match canonicalized_path_result {
            Ok(path) => String::from(path.to_str().unwrap()),
            Err(e) => {
                error!("Not able to find provided directory '{}'. Error: {}", &directory, e);
                exit(1)
            }
        }
    }

    fn set_timeout_in(socket: &UdpSocket) {
        socket.set_read_timeout(Option::from(Duration::new(SENDING_SOCKET_TIMEOUT_SECONDS, 0))).expect("Not able to set timeout.");
    }

    fn reset_timeout_in(socket: &UdpSocket) {
        socket.set_read_timeout(Option::None).expect("Not able to reset timeout.");
    }
}

#[cfg(test)]
mod tests {

    extern crate tempdir;
    use tftp::server::tests::tempdir::TempDir;
    use tftp::server::Server;
    use std::path::Path;
    use std::net::UdpSocket;
    use tftp::message::MessageEncoder;
    use tftp::message::Message;
    use tftp::codes::Mode;
    use std::net::SocketAddrV4;
    use std::net::Ipv4Addr;
    use tftp::message::MessageDecoder;
    use tftp::codes::ErrorCode;
    use std::thread;
    use std::time::Duration;
    use std::path::MAIN_SEPARATOR;

    #[test]
    fn add_trailing_slash_works() {
        assert_eq!(Server::add_trailing_path_separator("abc"), format!("{}{}", "abc", MAIN_SEPARATOR));
    }

    #[test]
    fn canonicalize_works() {
        // given
        let tmp_dir = TempDir::new("./abc").unwrap();

        // when
        let canonicalized_dir = Server::canonicalize(&string_from_path(tmp_dir.path()));

        // then
        assert_eq!(string_from_path(&tmp_dir.path().canonicalize().unwrap()), canonicalized_dir);
    }

    #[test]
    fn tidying_works() {
        assert_eq!(Server::tidy("abc"), "abc");
        assert_eq!(Server::tidy("abc.def"), "abc.def");
        assert_eq!(Server::tidy("abc..def"), "abcdef");
        assert_eq!(Server::tidy("abc...def"), "abc.def");
        assert_eq!(Server::tidy("./abc"), "./abc");
        assert_eq!(Server::tidy("./../../abc"), ".///abc");
        assert_eq!(Server::tidy("\\.\\./abc"), "abc");
        assert_eq!(Server::tidy("abc\\"), "abc");
        assert_eq!(Server::tidy("/etc/passwd"), "etc/passwd");
        assert_eq!(Server::tidy("/////etc/passwd"), "etc/passwd");
    }

    #[test]
    fn read_request_works() {
        // given
        let correct_read_request = Message::ReadRequest{ filename: s("ok.txt"), mode: Mode::NETASCII };

        // when
        let response = send_and_receive(9874, 9875, correct_read_request);

        // then
        match response {
            Message::DataMessage { block, data } => {
                assert_eq!(block, 1);
                assert_eq!(data, vec![79, 75]);
            },
            _ => assert!(false)
        }
    }

    #[test]
    fn read_request_for_non_existing_file_produces_error_works() {
        // given
        let read_request_for_non_existing_file = Message::ReadRequest{ filename: s("asd"), mode: Mode::NETASCII };

        // when
        let response = send_and_receive(9877, 9876, read_request_for_non_existing_file);

        // then
        match response {
            Message::ErrorMessage { error_code, error_message } => {
                assert_eq!(error_code, ErrorCode::FileNotFound);
                assert_eq!(error_message, "The file \'asd\' can not be served.");
            },
            _ => assert!(false)
        }
    }

    #[test]
    fn write_request_produces_error_works() {
        // given
        let write_request = Message::WriteRequest{ filename: s("asd"), mode: Mode::NETASCII };

        // when
        let response = send_and_receive(9873, 9872, write_request);

        // then
        match response {
            Message::ErrorMessage { error_code, error_message } => {
                assert_eq!(error_code, ErrorCode::NotDefined);
                assert_eq!(error_message, "Operation not implemented.");
            },
            _ => assert!(false)
        }
    }

    fn send_and_receive(client_port: u32, server_port: u32, message: Message) -> Message {
        let server_address = SocketAddrV4::new(Ipv4Addr::new(0, 0, 0, 0), server_port as u16);
        let mut client_buffer = [0; 2048];
        let client = UdpSocket::bind(format!("0.0.0.0:{}", client_port)).unwrap();
        let read_request_payload = MessageEncoder::encode(&message);

        thread::spawn(move || {
            // We start a new thread with the server and just let it run. It is being removed once we leave the context.
            let server = Server::from("0.0.0.0", server_port, "./sample_files");
            server.start();
        });

        let handle = thread::spawn(move || {
            thread::sleep(Duration::from_millis(200));
            client.send_to(&read_request_payload, &server_address).unwrap();
            client
        });

        // Since we moved the client into the thread, we're getting it back here.
        let client = handle.join().unwrap();
        let bytes_read = client.recv(&mut client_buffer).unwrap();

        MessageDecoder::decode(&mut client_buffer[0..bytes_read]).unwrap()
    }

    // Some helper methods

    fn s(s: &str) -> String {
        String::from(s)
    }

    fn string_from_path(p: &Path) -> String {
        p.display().to_string()
    }
}
