use std::fmt::Display;
use std::fmt::Formatter;

use tftp::core::fmt;

/// The `OpCode` is the main packet type determination mechanism.
#[derive(Debug)]
pub enum OpCode {
    RRQ,
    WRQ,
    DATA,
    ACK,
    ERROR
}

impl Display for OpCode {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.code())
    }
}

impl OpCode {
    pub fn code(&self) -> u8 {
        match *self {
            OpCode::RRQ => 1,
            OpCode::WRQ => 2,
            OpCode::DATA => 3,
            OpCode::ACK => 4,
            OpCode::ERROR => 5
        }
    }

    pub fn from(command: u16) -> Result<OpCode, String> {
        match command {
            1 => Ok(OpCode::RRQ),
            2 => Ok(OpCode::WRQ),
            3 => Ok(OpCode::DATA),
            4 => Ok(OpCode::ACK),
            5 => Ok(OpCode::ERROR),
            _ => Err(String::from("Unknown opcode."))
        }
    }
}

#[derive(Debug)]
pub enum Mode {
    NETASCII,
    OCTET
}

impl Display for Mode {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.name())
    }
}

impl Mode {
    pub fn from(mode: &str) -> Result<Mode, String> {
        match mode {
            "netascii" => Ok(Mode::NETASCII),
            "octet"    => Ok(Mode::OCTET),
            _          => Err(String::from("Unknown mode."))
        }
    }

    pub fn name(&self) -> &str {
        match *self {
            Mode::NETASCII => "netascii",
            Mode::OCTET => "octet"
        }
    }
}

///
/// Error Codes
///
/// ```
/// Value     Meaning
///
/// 0         Not defined, see error message (if any).
/// 1         File not found.
/// 2         Access violation.
/// 3         Disk full or allocation exceeded.
/// 4         Illegal TFTP operation.
/// 5         Unknown transfer ID.
/// 6         File already exists.
/// 7         No such user.
/// ```
/// Src: https://tools.ietf.org/html/rfc1350
#[derive(Debug, PartialEq, Copy, Clone)]
pub enum ErrorCode {
    NotDefined = 0,
    FileNotFound = 1,
    AccessViolation = 2,
    DiskFull = 3,
    IllegalTftpOperation = 4,
    UnknownTransferId = 5,
    FileAlreadyExists = 6,
    NoSuchUser = 7
}

impl ErrorCode {
    pub fn from(command: u16) -> Result<ErrorCode, String> {
        match command {
            0 => Ok(ErrorCode::NotDefined),
            1 => Ok(ErrorCode::FileNotFound),
            2 => Ok(ErrorCode::AccessViolation),
            3 => Ok(ErrorCode::DiskFull),
            4 => Ok(ErrorCode::IllegalTftpOperation),
            5 => Ok(ErrorCode::UnknownTransferId),
            6 => Ok(ErrorCode::FileAlreadyExists),
            7 => Ok(ErrorCode::NoSuchUser),
            _ => Err(String::from("Unknown error code."))
        }
    }
}

#[cfg(test)]
mod tests {
    use tftp::codes::OpCode;
    use tftp::codes::Mode;
    use tftp::codes::ErrorCode;

    #[test]
    fn opcode_get_code_works() {
        assert_eq!(OpCode::RRQ.code(), 1);
        assert_eq!(OpCode::WRQ.code(), 2);
        assert_eq!(OpCode::DATA.code(), 3);
        assert_eq!(OpCode::ACK.code(), 4);
        assert_eq!(OpCode::ERROR.code(), 5);
    }

    #[test]
    fn mode_get_name_works() {
        assert_eq!(Mode::NETASCII.name(), "netascii");
        assert_eq!(Mode::OCTET.name(), "octet");
    }

    #[test]
    fn mode_from_works() {
        assert_eq!(Mode::from("netascii").unwrap().name(), Mode::NETASCII.name());
        assert_eq!(Mode::from("octet").unwrap().name(), Mode::OCTET.name());

        assert_eq!(Mode::from("whee").err().unwrap(), "Unknown mode.");
        assert_eq!(Mode::from("netascii   ").err().unwrap(), "Unknown mode.");
    }

    #[test]
    fn error_code_from_works() {
        assert_eq!(ErrorCode::from(0).unwrap(), ErrorCode::NotDefined);
        assert_eq!(ErrorCode::from(1).unwrap(), ErrorCode::FileNotFound);
        assert_eq!(ErrorCode::from(2).unwrap(), ErrorCode::AccessViolation);
        assert_eq!(ErrorCode::from(3).unwrap(), ErrorCode::DiskFull);
        assert_eq!(ErrorCode::from(4).unwrap(), ErrorCode::IllegalTftpOperation);
        assert_eq!(ErrorCode::from(5).unwrap(), ErrorCode::UnknownTransferId);
        assert_eq!(ErrorCode::from(6).unwrap(), ErrorCode::FileAlreadyExists);
        assert_eq!(ErrorCode::from(7).unwrap(), ErrorCode::NoSuchUser);

        assert_eq!(ErrorCode::from(42).err().unwrap(), "Unknown error code.");
    }

}

