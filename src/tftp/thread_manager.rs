use std::thread;
use std::sync::mpsc::channel;
use chrono::prelude::*;
use std::sync::mpsc::Receiver;
use std::sync::mpsc::Sender;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;

/// The `ThreadManager` executes jobs in new threads and controls that not too much threads (to be
/// specified with the `capacity` property) are run at the same time.
///
/// If run over capacity, the `ThreadManager` blocks the execution of a new thread until a slot is
/// available.
pub struct ThreadManager {
    capacity: usize,
    tx: Sender<()>,
    rx: Receiver<()>,
    thread_count: Arc<AtomicUsize>
}

impl ThreadManager {

    /// Create instance of the `ThreadManager` with provided capacity.
    pub fn new(capacity: usize) -> ThreadManager {
        assert!(0 < capacity);
        debug!("Starting thread manager with size {}.", capacity);

        let (tx,rx) = channel();
        ThreadManager { capacity, tx, rx, thread_count: Arc::new(AtomicUsize::new(0)) }
    }

    pub fn execute<J>(&mut self, job: J) -> thread::JoinHandle<()> where J: FnOnce() + Send + 'static {

        while !self.has_capacity() {
            // We are waiting for a thread to join.
            self.rx.recv().unwrap();
        }
        self.thread_count.fetch_add(1, Ordering::SeqCst);
        Thread::start_with(job, &self.tx, &self.thread_count).handle
    }

    fn has_capacity(&self) -> bool {
        self.active_threads() < self.capacity
    }

    fn active_threads(&self) -> usize {
        self.thread_count.load(Ordering::Relaxed)
    }
}

/// The `Thread` holds information about the life state of a thread.
struct Thread {
    handle: thread::JoinHandle<()>
}

impl Thread {

    fn start_with<J>(job: J, tx: &Sender<()>, thread_count: &Arc<AtomicUsize>) -> Thread where J: FnOnce() + Send + 'static {
        let starting_time = Local::now();
        let shared_sender = tx.clone();
        let shared_thread_count = thread_count.clone();

        let handle = thread::spawn(move|| {
            debug!("Spawning {:?}.", thread::current().id());

            // Doing the actual work
            job();

            shared_thread_count.fetch_sub(1, Ordering::SeqCst);
            shared_sender.send(()).expect("Not able to send.");

            debug!("Collecting {:?} after {} ms.", thread::current().id(), (Local::now() - starting_time).num_milliseconds());
        });

        Thread { handle }
    }
}



#[cfg(test)]
mod tests {

    use tftp::thread_manager::ThreadManager;
    use std::thread;
    use std::time::Duration;
    use tftp::chrono::Local;

    #[test]
    #[should_panic]
    fn capacity_positive_works() {
        ThreadManager::new(0);
    }

    #[test]
    fn has_capacity_works() {
        // given
        let mut thread_manager = ThreadManager::new(1);

        // when
        thread_manager.execute(|| sleep_for(20));

        // then
        assert_eq!(thread_manager.capacity, 1);
        assert_eq!(thread_manager.has_capacity(), false);
    }

    #[test]
    fn capacity_enforcement_works() {
        // given
        let mut thread_manager = ThreadManager::new(2);

        // when
        let starting_time = Local::now();
        thread_manager.execute(|| sleep_for(20));
        thread_manager.execute(|| sleep_for(20));
        thread_manager.execute(|| sleep_for(1));

        // then
        assert!(20 <= (Local::now() - starting_time).num_milliseconds());
    }

    #[test]
    fn race_condition_prevention_works() {
        // given
        let mut thread_manager = ThreadManager::new(1);

        // when
        let t01 = thread_manager.execute(|| sleep_for(0));
        let t02 = thread_manager.execute(|| sleep_for(0));
        let t03 = thread_manager.execute(|| sleep_for(0));
        let t04 = thread_manager.execute(|| sleep_for(0));
        let t05 = thread_manager.execute(|| sleep_for(0));
        let t06 = thread_manager.execute(|| sleep_for(0));
        let t07 = thread_manager.execute(|| sleep_for(0));
        let t08 = thread_manager.execute(|| sleep_for(0));
        let t09 = thread_manager.execute(|| sleep_for(0));
        let t10 = thread_manager.execute(|| sleep_for(0));

        let t11 = thread_manager.execute(|| sleep_for(0));
        let t12 = thread_manager.execute(|| sleep_for(0));
        let t13 = thread_manager.execute(|| sleep_for(0));
        let t14 = thread_manager.execute(|| sleep_for(0));
        let t15 = thread_manager.execute(|| sleep_for(0));
        let t16 = thread_manager.execute(|| sleep_for(0));
        let t17 = thread_manager.execute(|| sleep_for(0));
        let t18 = thread_manager.execute(|| sleep_for(0));
        let t19 = thread_manager.execute(|| sleep_for(0));
        let t20 = thread_manager.execute(|| sleep_for(0));

        t01.join().unwrap_or(());
        t02.join().unwrap_or(());
        t03.join().unwrap_or(());
        t04.join().unwrap_or(());
        t05.join().unwrap_or(());
        t06.join().unwrap_or(());
        t07.join().unwrap_or(());
        t08.join().unwrap_or(());
        t09.join().unwrap_or(());
        t10.join().unwrap_or(());
        t11.join().unwrap_or(());
        t12.join().unwrap_or(());
        t13.join().unwrap_or(());
        t14.join().unwrap_or(());
        t15.join().unwrap_or(());
        t16.join().unwrap_or(());
        t17.join().unwrap_or(());
        t18.join().unwrap_or(());
        t19.join().unwrap_or(());
        t20.join().unwrap_or(());

        // then
        assert_eq!(thread_manager.active_threads(), 0);
    }

    fn sleep_for (milliseconds: u64) {
        thread::sleep(Duration::from_millis(milliseconds));
    }

}
