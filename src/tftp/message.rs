use std::io::Cursor;

use tftp::byteorder::{ReadBytesExt, WriteBytesExt};
use tftp::byteorder::BigEndian;
use tftp::codes::OpCode;
use tftp::codes::Mode;
use tftp::codes::ErrorCode;
use std::fmt;

const NULL_BYTE: &[u8] = &[0];

/// The Message parts are based on the following TFTP formats:
///
/// ```
///    Type   Op #     Format without header
///
///           2 bytes    string   1 byte     string   1 byte
///           -----------------------------------------------
///    RRQ/  | 01/02 |  Filename  |   0  |    Mode    |   0  |
///    WRQ    -----------------------------------------------
///           2 bytes    2 bytes       n bytes
///           ---------------------------------
///    DATA  | 03    |   Block #  |    Data    |
///           ---------------------------------
///           2 bytes    2 bytes
///           -------------------
///    ACK   | 04    |   Block #  |
///           --------------------
///           2 bytes  2 bytes        string    1 byte
///           ----------------------------------------
///    ERROR | 05    |  ErrorCode |   ErrMsg   |   0  |
///           ----------------------------------------
/// ```
/// Src: https://tools.ietf.org/html/rfc1350.html
///
pub enum Message {
    ReadRequest { filename: String, mode: Mode },
    WriteRequest { filename: String, mode:Mode },
    DataMessage { block: u16, data: Vec<u8> },
    Acknowledgement { block: u16 },
    ErrorMessage { error_code: ErrorCode, error_message: String }
}

impl fmt::Display for Message {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let printable = match *self {
            Message::ReadRequest { ref filename, ref mode } => format!("RRQ [ filename: {}, mode: {} ]", filename, mode),
            Message::WriteRequest { ref filename, ref mode } => format!("WRQ [ filename: {}, mode: {} ]", filename, mode),
            Message::DataMessage { block, ref data } => format!("DATA [ block: {}, as string: '{}', as bytes: <{}> ]", block, String::from_utf8_lossy(data), to_hex_vector(data) ),
            Message::Acknowledgement { block } => format!("ACK [ block: {} ]", block),
            Message::ErrorMessage { ref error_code, ref error_message } => format!("ERROR [ error_code: {:?}, error_message: {} ]", error_code, error_message)
        };
        write!(f, "{}", printable)
    }
}

fn to_hex_vector(byte_array: &[u8]) -> String {
    let mut result = vec![];
    for i in byte_array {
        result.push(format!("{:02X}", i))
    }
    result.join(", ")
}

impl Message {

    fn _filename(&self) -> Option<&String> {
        match *self {
            Message::ReadRequest { ref filename, .. } => Some(filename),
            Message::WriteRequest { ref filename, .. } => Some(filename),
            Message::DataMessage { .. } => None,
            Message::Acknowledgement { .. } => None,
            Message::ErrorMessage { .. } => None
        }
    }

    fn _mode(&self) -> Option<&Mode> {
        match *self {
            Message::ReadRequest { ref mode, .. } => Some(mode),
            Message::WriteRequest { ref mode, .. } => Some(mode),
            Message::DataMessage { .. } => None,
            Message::Acknowledgement { .. } => None,
            Message::ErrorMessage { .. } => None
        }
    }

    fn _block(&self) -> Option<u16> {
        match *self {
            Message::ReadRequest {  .. } => None,
            Message::WriteRequest { .. } => None,
            Message::DataMessage { block, .. } => Some(block),
            Message::Acknowledgement { block, .. } => Some(block),
            Message::ErrorMessage { .. } => None
        }
    }

    fn _data(&self) -> Option<&Vec<u8>> {
        match *self {
            Message::ReadRequest {  .. } => None,
            Message::WriteRequest { .. } => None,
            Message::DataMessage { ref data, .. } => Some(data),
            Message::Acknowledgement { .. } => None,
            Message::ErrorMessage { .. } => None
        }
    }

    fn _error_code(&self) -> Option<&ErrorCode> {
        match *self {
            Message::ErrorMessage { ref error_code, .. } => Some(error_code),
            _ => None
        }
    }

    fn _error_message(&self) -> Option<&String> {
        match *self {
            Message::ErrorMessage { ref error_message, .. } => Some(error_message),
            _ => None
        }
    }
}

pub struct MessageDecoder {
}

impl MessageDecoder {

    /// Decodes a byte buffer received from the socket.
    pub fn decode(buffer: &[u8]) -> Result<Message, String> {
        let opcode_result = MessageDecoder::extract_opcode_from(&buffer[0..2]);
        match opcode_result {
            Ok(opcode) => {
                match opcode {
                    OpCode::RRQ => {
                        let parts = MessageDecoder::split_up(buffer);
                        let filename = MessageDecoder::extract_string_from(&parts[1]);
                        let mode = MessageDecoder::extract_transfer_mode_from(&parts[2]);
                        Result::Ok(Message::ReadRequest { filename, mode })
                    },
                    OpCode::WRQ => {
                        let parts = MessageDecoder::split_up(buffer);
                        let filename = MessageDecoder::extract_string_from(&parts[1]);
                        let mode = MessageDecoder::extract_transfer_mode_from(&parts[2]);
                        Result::Ok(Message::WriteRequest { filename, mode })
                    },
                    OpCode::DATA => {
                        let block = MessageDecoder::byte_pair_to_integer(&buffer[2..4]);
                        let data = Vec::from(&buffer[4..]);
                        Result::Ok(Message::DataMessage { block, data })
                    },
                    OpCode::ACK => {
                        let block = MessageDecoder::byte_pair_to_integer(&buffer[2..4]);
                        Result::Ok(Message::Acknowledgement { block })
                    },
                    OpCode::ERROR => {
                        let error_code = MessageDecoder::extract_error_code_from(&buffer[2..4]);
                        let message_end_index = MessageDecoder::next_zero_index_of(4, &buffer);
                        let error_message = MessageDecoder::extract_string_from(&buffer[4..message_end_index]);
                        Result::Ok(Message::ErrorMessage { error_code, error_message })
                    }
                }
            },
            Err(msg) => {
                Result::Err(msg)
            }
        }
    }

    /// Splits the input buffer up into the single parts and returns a vector with them.
    ///
    /// ```
    ///    2 bytes     string    1 byte     string   1 byte
    ///    ------------------------------------------------
    ///    | Opcode |  Filename  |   0  |    Mode    |   0  |
    ///    ------------------------------------------------
    ///
    ///    Figure 5-1: RRQ/WRQ packet
    /// ```
    /// Src: https://tools.ietf.org/html/rfc1350
    ///
    fn split_up(buffer: &[u8]) -> Vec<&[u8]> {
        let mut result: Vec<&[u8]> = vec![];

        // The opcode are the first two bytes.
        result.push(&buffer[0..2]);

        // Then, the filename in question is all the bytes to the next zero byte.
        let filename_end_index = MessageDecoder::next_zero_index_of(2, &buffer);
        result.push(&buffer[2..filename_end_index]);

        // The mode is stored in the following bytes until the next zero byte.
        let mode_end_index = MessageDecoder::next_zero_index_of(filename_end_index + 1, &buffer);
        result.push(&buffer[filename_end_index+1..mode_end_index]);

        result
    }

    fn next_zero_index_of(start_index: usize, buffer: &[u8]) -> usize {
        let mut zero_index = 0;
        for (i, item) in buffer[start_index..].iter().enumerate() {
            if *item == 0 {
                zero_index = i;
                break;
            }
        }
        zero_index + start_index as usize
    }

    fn extract_opcode_from(buffer_fragment: &[u8]) -> Result<OpCode, String> {
        OpCode::from(MessageDecoder::byte_pair_to_integer(&buffer_fragment))
    }

    fn extract_string_from(buffer_fragment: &[u8]) -> String {
        MessageDecoder::string_from_byte_buffer(&buffer_fragment)
    }

    fn string_from_byte_buffer(buffer: &[u8]) -> String {
        String::from(String::from_utf8_lossy(&buffer))
    }

    fn extract_transfer_mode_from(buffer_fragment: &[u8]) -> Mode {
        Mode::from(&MessageDecoder::string_from_byte_buffer(&buffer_fragment)).unwrap()
    }

    fn extract_error_code_from(buffer_fragment: &[u8]) -> ErrorCode {
        ErrorCode::from(MessageDecoder::byte_pair_to_integer(&buffer_fragment)).unwrap()
    }

    fn byte_pair_to_integer(byte_pair: &[u8]) -> u16 {
        assert_eq!(byte_pair.len(), 2);

        let mut rdr = Cursor::new(byte_pair);
        rdr.read_u16::<BigEndian>().unwrap()
    }
}

pub struct MessageEncoder {
}

impl MessageEncoder {

    /// Encodes a message to a transmittable byte array.
    pub fn encode(message: &Message) -> Vec<u8> {
        match *message {
            Message::ReadRequest { ref filename, ref mode } => MessageEncoder::read_request_from(&filename, &mode),
            Message::WriteRequest { ref filename, ref mode } => MessageEncoder::write_request_from(&filename, &mode),
            Message::DataMessage { block, ref data } => MessageEncoder::data_from(block, &data),
            Message::Acknowledgement { block } => MessageEncoder::acknowledge_from(block),
            Message::ErrorMessage { error_code, ref error_message } => MessageEncoder::error_from(error_code, &error_message)
        }
    }

    fn read_request_from(filename: &str, mode: &Mode) -> Vec<u8> {
        MessageEncoder::combine(&[
            &MessageEncoder::integer_to_byte_pair(u16::from(OpCode::RRQ.code())),
            &filename.as_bytes(),
            NULL_BYTE,
            &mode.name().as_bytes(),
            NULL_BYTE
        ])
    }

    fn write_request_from(filename: &str, mode: &Mode) -> Vec<u8> {
        MessageEncoder::combine(&[
            &MessageEncoder::integer_to_byte_pair(u16::from(OpCode::WRQ.code())),
            &filename.as_bytes(),
            NULL_BYTE,
            &mode.name().as_bytes(),
            NULL_BYTE
        ])
    }

    fn data_from(block_number: u16, data: &[u8]) -> Vec<u8> {
        MessageEncoder::combine(&[
            &MessageEncoder::integer_to_byte_pair(u16::from(OpCode::DATA.code())),
            &MessageEncoder::integer_to_byte_pair(block_number),
            &data
        ])
    }

    fn acknowledge_from(block_number: u16) -> Vec<u8> {
        MessageEncoder::combine(&[
            &MessageEncoder::integer_to_byte_pair(u16::from(OpCode::ACK.code())),
            &MessageEncoder::integer_to_byte_pair(block_number)
        ])
    }

    fn error_from(code: ErrorCode, message: &str) -> Vec<u8> {
        MessageEncoder::combine(&[
            &MessageEncoder::integer_to_byte_pair(u16::from(OpCode::ERROR.code())),
            &MessageEncoder::integer_to_byte_pair(code as u16),
            message.as_bytes(),
            NULL_BYTE
        ])
    }

    fn combine(args: &[&[u8]]) -> Vec<u8> {
        let mut payload: Vec<u8> =  vec![];
        for arg in args {
            payload.extend_from_slice(arg);
        }
        payload
    }

    fn integer_to_byte_pair(integer: u16) -> Vec<u8> {
        let mut wtr = vec![];
        wtr.write_u16::<BigEndian>(integer).unwrap();
        wtr
    }
}

#[cfg(test)]
mod tests {
    use tftp::message::MessageDecoder;
    use tftp::message::MessageEncoder;
    use tftp::message::Message;
    use tftp::codes::Mode;
    use tftp::codes::ErrorCode;

    #[test]
    fn integer_to_byte_pair_works() {
        assert_eq!(MessageDecoder::byte_pair_to_integer(&[0, 0]), 0);
        assert_eq!(MessageDecoder::byte_pair_to_integer(&[0, 1]), 1);
        assert_eq!(MessageDecoder::byte_pair_to_integer(&[0, 67]), 67);
        assert_eq!(MessageDecoder::byte_pair_to_integer(&[0, 255]), 255);
        assert_eq!(MessageDecoder::byte_pair_to_integer(&[1, 0]), 256);
        assert_eq!(MessageDecoder::byte_pair_to_integer(&[255, 255]), 65535);
    }

    #[test]
    fn byte_pair_to_integer_works() {
        assert_eq!(MessageEncoder::integer_to_byte_pair(0), &[0, 0]);
        assert_eq!(MessageEncoder::integer_to_byte_pair(1), &[0, 1]);
        assert_eq!(MessageEncoder::integer_to_byte_pair(67), &[0, 67]);
        assert_eq!(MessageEncoder::integer_to_byte_pair(255), &[0, 255]);
        assert_eq!(MessageEncoder::integer_to_byte_pair(256), &[1, 0]);
        assert_eq!(MessageEncoder::integer_to_byte_pair(65535), &[255, 255]);
    }

    #[test]
    fn encoding_decoding_read_requests_works() {
        // given
        let request = Message::ReadRequest { filename: String::from("myFile"), mode: Mode::NETASCII };

        // when
        let other_request = MessageDecoder::decode(&MessageEncoder::encode(&request)).unwrap();

        // then
        assert_eq!(&request._filename().unwrap(), &other_request._filename().unwrap());
        assert_eq!(&request._mode().unwrap().name(), &other_request._mode().unwrap().name());
    }

    #[test]
    fn decoding_read_requests_work() {
        // given
        let received_bytes = vec![0, 1, 97, 98, 99, 0, 110, 101, 116, 97, 115, 99, 105, 105, 0];

        // when
        let decoded_message = MessageDecoder::decode(&received_bytes).unwrap();

        // then
        assert_eq!(decoded_message._filename().unwrap(), "abc");
        assert_eq!(decoded_message._mode().unwrap().name(), "netascii");
    }

    #[test]
    fn encoding_decoding_write_requests_works() {
        // given
        let request = Message::WriteRequest { filename: String::from("myFile"), mode: Mode::NETASCII };

        // when
        let other_request = MessageDecoder::decode(&MessageEncoder::encode(&request)).unwrap();

        // then
        assert_eq!(request._filename().unwrap(), other_request._filename().unwrap());
        assert_eq!(request._mode().unwrap().name(), other_request._mode().unwrap().name());
    }

    #[test]
    fn decoding_write_requests_work() {
        // given
        let received_bytes = vec![0, 2, 97, 98, 99, 0, 110, 101, 116, 97, 115, 99, 105, 105, 0];

        // when
        let decoded_message = MessageDecoder::decode(&received_bytes).unwrap();

        // then
        assert_eq!(decoded_message._filename().unwrap(), "abc");
        assert_eq!(decoded_message._mode().unwrap().name(), "netascii");
    }

    #[test]
    fn encoding_decoding_data_message_works() {
        // given
        let request = Message::DataMessage { block: 35, data: vec![1, 2, 3, 4, 5, 6] };

        // when
        let other_request = MessageDecoder::decode(&MessageEncoder::encode(&request)).unwrap();

        // then
        assert_eq!(request._block().unwrap(), other_request._block().unwrap());
        assert_eq!(request._data().unwrap(), other_request._data().unwrap());
    }

    #[test]
    fn decoding_data_requests_work() {
        // given
        let received_bytes = vec![0, 3, 0, 52, 1, 2, 3];

        // when
        let decoded_message = MessageDecoder::decode(&received_bytes).unwrap();

        // then
        assert_eq!(decoded_message._block().unwrap(), 52 as u16);
        assert_eq!(*decoded_message._data().unwrap(), vec![1, 2, 3]);
    }

    #[test]
    fn encoding_decoding_acknowledgement_works() {
        // given
        let request = Message::Acknowledgement { block: 35 };

        // when
        let other_request = MessageDecoder::decode(&MessageEncoder::encode(&request)).unwrap();

        // then
        assert_eq!(request._block().unwrap(), other_request._block().unwrap());
    }

    #[test]
    fn decoding_acknowledge_requests_work() {
        // given
        let received_bytes = vec![0, 4, 0, 42];

        // when
        let decoded_message = MessageDecoder::decode(&received_bytes).unwrap();

        // then
        assert_eq!(decoded_message._block().unwrap(), 42 as u16);
    }

    #[test]
    fn encoding_decoding_error_message_works() {
        // given
        let request = Message::ErrorMessage { error_code: ErrorCode::NotDefined, error_message: String::from("my message") };

        // when
        let other_request = MessageDecoder::decode(&MessageEncoder::encode(&request)).unwrap();

        // then
        assert_eq!(request._error_code().unwrap(), other_request._error_code().unwrap());
        assert_eq!(request._error_message().unwrap(), other_request._error_message().unwrap());
    }

    #[test]
    fn decoding_error_requests_work() {
        // given
        let received_bytes = vec![0, 5, 0, 3, 97, 98, 99, 0];

        // when
        let decoded_message = MessageDecoder::decode(&received_bytes).unwrap();

        // then
        assert_eq!(*decoded_message._error_code().unwrap(), ErrorCode::DiskFull);
        assert_eq!(decoded_message._error_message().unwrap(), "abc");
    }
}
