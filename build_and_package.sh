#!/bin/bash
# Shell script to perform a release build and package it into a respective, os-specific archive.

# -- Collecting data

PRODUCT_NAME="tftp-ad-hoc-server"
echo "Packaging" ${PRODUCT_NAME}

PROJECT_PATH=.
echo "Project path:" ${PROJECT_PATH}

TARGET_DIRECTORY=target
echo "Target directory:" ${TARGET_DIRECTORY}

VERSION=`grep version Cargo.toml | sed 's/.*version = "\(.*\)".*/\1/'`
echo "Product version:" ${VERSION}

BINARY_NAME=${PRODUCT_NAME}

PLATFORM=`uname`
if [[ ${PLATFORM} = *"WIN"* ]] || [[ ${PLATFORM} = *"MINGW"* ]] || [[ ${PLATFORM} = *"CYGWIN"* ]];
then
  OS_STRING="windows"
  BINARY_NAME="$PRODUCT_NAME.exe"

  if [[ `uname -a` = *"x86_64"* ]];
  then
    ARCHITECTURE="-x64"
  else
    ARCHITECTURE=""
  fi

elif [[ ${PLATFORM} = *"Linux"* ]];
then
  OS_STRING="linux"
  ARCHITECTURE="-`uname -p`"

elif [[ ${PLATFORM} = *"Darwin"* ]];
then
  OS_STRING="mac"
  ARCHITECTURE=""

else
  OS_STRING=`uname -o`
fi
echo "Platform:" ${OS_STRING}
echo "Binary name:" ${BINARY_NAME}
echo "Architecture:" ${ARCHITECTURE}

ARCHIVE_NAME="$PRODUCT_NAME-$VERSION-$OS_STRING$ARCHITECTURE"
echo "Archive name:" ${ARCHIVE_NAME}

ARCHIVE_PATH=${PROJECT_PATH}/${TARGET_DIRECTORY}/${ARCHIVE_NAME}


# -- Cleaning up potential existing directory and creating it anew

echo "Cleaning build"
cargo clean

echo "Creating target directory:" ${ARCHIVE_PATH}
mkdir -p ${ARCHIVE_PATH}

# -- Creating a release build

echo "Building release"
cargo build --release

# -- Copying the files into the archive path

echo "Copying files"
cp ${PROJECT_PATH}/${TARGET_DIRECTORY}/release/${BINARY_NAME} ${ARCHIVE_PATH}
cp ${PROJECT_PATH}/CHANGELOG ${ARCHIVE_PATH}
cp ${PROJECT_PATH}/LICENSE ${ARCHIVE_PATH}
cp ${PROJECT_PATH}/README.md ${ARCHIVE_PATH}

# -- Stripping binary

if [[ ${OS_STRING} = "linux" ]];
then
    echo "Stripping debug info from binary"
    /usr/bin/strip ${ARCHIVE_PATH}/${BINARY_NAME}
fi

# -- Zipping archive

ZIP_FILE=${ARCHIVE_NAME}.zip
echo "Creating archive:" ${ZIP_FILE}
pushd ${PROJECT_PATH}/${TARGET_DIRECTORY}
zip -r ${ZIP_FILE} ${ARCHIVE_NAME}
popd
